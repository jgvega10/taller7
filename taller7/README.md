1. ¿Es	posible	lograr	el	mismo	resultado	con	el	pre-orden	y	pos-orden?	
Responda	en	el	archivo	README.md.

-  Si se recibe la secuencia del recorrido del arbol binario en forma de pre-orden y pos-orden, el arbol NO, puede 
   ser reconstruido, ya que esta informacion no es suficiente ya que existen alternativas diferentes al momento
   de querer recontruir el arbol. Por lo tanto no es posible obtener los mismos resultados

2. ¿Es	posible	lograr	el	mismo	resultado	con	el	in-orden	y	pos-orden?	
Responda	en	el	archivo	README.md.	

-  Si se recibe la secuencia del recorrido del arbol binario en forma de in-orden y pos-orden, el arbol
   binario puede ser reconstruido, gracias a estas secuencias. Por lo tanto si se puede obtener los mismos resultados.