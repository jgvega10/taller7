package taller.test;

import taller.Arbol;
import junit.framework.TestCase;

public class MainTest extends TestCase
{
   
	private Arbol arbol;
	public void setUpEscenario1()
	{
		arbol = new Arbol();
	}
	
	public void validarRaiz()
	{
		setUpEscenario1();
		String nomArc = arbol.darNombreArch();
		
		if(nomArc.equalsIgnoreCase("ejemplo"))
		{
			String p = arbol.darRaizNodo().darValor();
			assertEquals("El arbol no se construyo debidamente","F", p);
		}
		else if(nomArc.equalsIgnoreCase("prueba1"))
		{
			String p = arbol.darRaizNodo().darValor();
			assertEquals("El arbol no se construyo debidamente","A", p);
		}
		else if(nomArc.equalsIgnoreCase("prueba2"))
		{
			String p = arbol.darRaizNodo().darValor();
			assertEquals("El arbol no se construyo debidamente","J", p);
		}
		else if(nomArc.equalsIgnoreCase("NombreApellido"))
		{
			String p = arbol.darRaizNodo().darValor();
			assertEquals("El arbol no se construyo debidamente","J", p);
		}
	}
	
  public void testSample() 
  {
    assertEquals(3, 3);
  }
  
  
  
}