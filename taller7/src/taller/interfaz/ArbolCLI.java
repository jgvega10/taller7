package taller.interfaz;

import java.util.Scanner;

import taller.Arbol;

public class ArbolCLI {

	private Scanner in;
	private Arbol principal;

	public ArbolCLI()
	{
		in = new Scanner(System.in);
		principal = new Arbol();
	}

	public void mainMenu()
	{
		Arbol a = new Arbol();
		a.darListaInOrden().agregar("A");
		a.darListaInOrden().agregar("B");
		a.darListaInOrden().agregar("C");
		a.darListaInOrden().agregar("D");
		a.darListaInOrden().agregar("E");
		a.darListaInOrden().agregar("F");
		a.darListaInOrden().agregar("G");
		a.darListaInOrden().agregar("H");

		a.darListapreOrden().agregar("F");
		a.darListapreOrden().agregar("B");
		a.darListapreOrden().agregar("A");
		a.darListapreOrden().agregar("D");
		a.darListapreOrden().agregar("C");
		a.darListapreOrden().agregar("E");
		a.darListapreOrden().agregar("G");
		a.darListapreOrden().agregar("I");
		a.darListapreOrden().agregar("H");
		
		a.construirArbol();

		a.preOrden();
		System.out.println();
		a.inOrden();  
		System.out.println();

		boolean finish = false;
		while(!finish)
		{	
//			Screen.clear();
			System.out.println("------------------------------------------");
			System.out.println("-                                        -");
			System.out.println("-           Siembra de árboles           -");
			System.out.println("-                                        -");
			System.out.println("------------------------------------------");
			System.out.println("EL sistema para la plantación de árboles binarios\n");

			System.out.println("Menú principal:");
			System.out.println("-----------------");
			System.out.println("1. Cargar archivo con semillas");
			System.out.println("2. Salir");
			System.out.print("\nSeleccione una opción: ");
			int opt1 = in.nextInt();
			switch(opt1)
			{
			case 1:
				recibirArchivo();
				finish = true;
				break;
			case 2:
				finish = true;
				break;
			default:
				break;
			}
		}
	}

	public void recibirArchivo()
	{
		boolean finish = false;
		while(!finish)
		{
//			Screen.clear();
			System.out.println("Recuerde que el archivo a cargar");
			System.out.println("debe ser un archivo properties");
			System.out.println("que tenga la propiedad in-orden,");
			System.out.println("la propiedad pre-orden (donde los ");
			System.out.println("elementos estén separados por comas) y");
			System.out.println("que esté guardado en la carpeta data.");
			System.out.println("");
			System.out.println("Introduzca el nombre del archivo:");
			System.out.println("----------------------------------------------------");

			// TODO Leer el archivo .properties 
			System.out.println("NOTA:ESCRIBIR UNICAMNETE EL NOMBRE DEL ARCHIVO !!!!!!!!!!");
			System.out.println("EJ: ejemplo.properties ------ poner: ejemplo");
			String arch  = in.next();
			String nombre = "data/"+arch+".properties";

			try
			{
				principal.cargarArchivo(nombre);  
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}


			try {

				// TODO cargarArchivo
				// TODO Reconstruir árbol  
				principal.crearArchivo();

				System.out.println("Ha plantado el árbol con éxito!\nPara verlo, dirijase a /data/arbolPlantado.json");
				System.out.println("Nota: ejecute Refresh (Clic derecho - Refresh) sobre la carpeta /data/ para actualizar y visualizar el archivo JSON");
				System.out.println("Presione 1 para salir");
				in.next();
				finish=true;

			} catch (Exception e) {
				System.out.println("Hubo un problema cargando el archivo:");
				System.out.println(e.getMessage());
				e.printStackTrace();

			}
		}
	}
} 