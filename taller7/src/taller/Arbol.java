package taller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import org.json.simple.JSONObject;

import taller.interfaz.IReconstructorArbol;

public class Arbol implements IReconstructorArbol
{
	private Nodo raiz;
	private String nombreArch;
	private String preor;
	private String inor;
	private Pila<Nodo> pila;
	private  Lista listaIn;
	private Lista listaPre;

	public Arbol()
	{
		raiz = null;
		pila = new Pila();
		listaIn = new Lista();
		listaPre = new Lista();
	}
	public Nodo darRaizNodo()
	{
		return raiz;
	}
	public Lista darListaInOrden()
	{
		return listaIn;
	}
	public Lista darListapreOrden()
	{
		return listaPre;
	}
	public boolean aLaIzquierda(String pV1, String pV2)
	{
		int a = listaIn.posElemento(pV1);
		int b = listaIn.posElemento(pV2);

		if(a<b )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public void construirArbol()
	{
		int pre = 0;
		int last = listaPre.posElemento(listaPre.darUltimoEnLista());

		Nodo t = new Nodo(listaPre.get(pre));
		Nodo temp = t;

		int ac = pre;

		pila.push(temp);
		pre++;

		while(pre<= last)
		{
			if(aLaIzquierda(listaPre.get(pre),listaPre.get(ac) ))
			{
				temp.izq = new Nodo(listaPre.get(pre));
				temp = temp.izq;
				pila.push(temp);
				ac = pre;
				pre++;
			}
			else
			{
				Nodo prev = null;
				while(true)
				{
					temp = pila.pop();
					if(!aLaIzquierda(listaPre.get(pre), temp.darValor()))
					{
						prev = temp;
					}
					else
					{
						pila.push(temp);
						temp = prev;
						break;
					}
					if(pila.vacio())
					{
						break;
					}
				}
				temp.der = new Nodo(listaPre.get(pre));
				temp = temp.der;
				pila.push(temp);
				ac = pre;
				pre++;
			}
		}
		raiz = t;
	}

	public void preOrden()
	{
		preOrden(raiz);
	}

	public void preOrden(Nodo node)
	{
		if(node==null )
		{
			return;
		}

		System.out.print(node.darValor()+",");
		preOrden(node.izq);

		preOrden(node.der);
	}

	public void inOrden()
	{
		inOrden(raiz);
		System.out.println();
	}

	public void inOrden(Nodo node)
	{
		if(node==null )
		{
			return;
		}

		inOrden(node.izq);
		System.out.print(node.darValor()+",");
		inOrden(node.der);
	}

	public class Nodo
	{
		Nodo der;
		Nodo izq;

		String valor;

		public Nodo(String pVal)
		{
			this.valor = pVal;
		}

		public Nodo darDerecho()
		{
			return der;
		}
		public Nodo darIzquierdo()
		{
			return izq;
		}
		public String darValor()
		{
			return valor;
		}
	}

	public void cargarArchivo(String nombre) throws IOException 
	{
		Properties datos = new Properties();
		nombreArch = nombre;
		File archivo = new File(nombre);

		FileInputStream in = new FileInputStream(archivo);

		datos.load(in);
		in.close();
		String a = datos.getProperty("preorden");
		String h = datos.getProperty("inorden");
		preor = a;
		inor = h;
		reconstruir();
	}

	public void crearArchivo() throws FileNotFoundException,UnsupportedEncodingException
	{	
		JSONObject obj = new JSONObject();

		obj = createJSON(darRaizNodo());

		try
		{
			FileWriter file = new FileWriter("./data/plantado.json");
			file.write(obj.toJSONString());
			file.flush();
			file.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public JSONObject createJSON(Nodo nodo)
	{
		if(nodo == null)
		{
			return null;
		}
		JSONObject nuevo = new JSONObject();
		nuevo.put("Valor", ""+nodo.darValor()+"");

		nuevo.put("Hijo izquierdo", createJSON(nodo.izq));


		nuevo.put("Hijo derecho", createJSON(nodo.der));

		return nuevo;
	}

	public void reconstruir()
	{
		String[] preorArr = preor.split(",");
		String[] inArr = inor.split(",");

		for (int i = 0; i < preorArr.length; i++)
		{
			darListapreOrden().agregar(preorArr[i]);
			darListaInOrden().agregar(inArr[i]);
		}

		construirArbol();

		preOrden();
		System.out.println();

		inOrden(); 

	}

	public String darNombreArch()
	{
		return nombreArch;
	}
}














