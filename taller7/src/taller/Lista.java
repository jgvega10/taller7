package taller;

public class Lista 
{
	private Nodo primero;
	
	private Nodo ultimo;
	
	private int tam;
	
	
	public Lista()
	{
		primero = null;
		ultimo = null;
		tam = 0;
	}
	
	public Nodo darPrimero()
	{
		return primero;
	}
	
	public Nodo darUltimo()
	{
		return ultimo;
	}
	public String get(int index)
	{
		Nodo actual = primero;
		int i = 0;
		while(actual != null)
		{
			if(index == i)
			{
				return actual.darElemento();
			}
			actual = actual.darSiguiente();
			i++;
		}
		return null;
	}
	
	public int darTamaño()
	{
		return tam;
	}
	
	public boolean vacio()
	{
		boolean estaVacio = false;
		
		if(tam==0 && primero == null)
		{
			estaVacio = true;
		}
		
		return estaVacio;
	}
	
	public int posElemento(String elemento)
	{
		Nodo a = primero;
		int i = 0;
		while(a != null)
		{
			if(elemento.equalsIgnoreCase(a.darElemento()))
			{
				return i;
			}
			i++;
			a = a.darSiguiente();
		}
		
		return -1;
	}
	public void agregar(String pElemento)
	{
		Nodo agregado = new Nodo(pElemento);
		
		if(vacio())
		{
			primero = agregado;
			ultimo = agregado;
		}
		else
		{
			ultimo.insertar(agregado);
			ultimo = agregado;
		}
	}
	
	public String darUltimoEnLista()
	{
		Nodo res = primero;
		
		if(res == null)
		{
			return null;
		}
		while(res.darSiguiente() != null)
		{
			res = res.darSiguiente();
		}
		return res.darElemento();
	}
	
	public class Nodo
	{
		private Nodo siguiente;
		
		private Nodo anterior;
		
		private String elemento;
		
		public Nodo(String pElemento)
		{
			siguiente = null;
			anterior = null;
			elemento = pElemento;
		}
		
		public Nodo darSiguiente()
		{
			return siguiente;
		}
		
		public Nodo darAnterior()
		{
			return anterior;
		}
		
		public String darElemento()
		{
			return elemento;
		}
		
		public void insertar(Nodo pSig)
		{
			siguiente = pSig;
			pSig.anterior = this;
		}
	}

}
